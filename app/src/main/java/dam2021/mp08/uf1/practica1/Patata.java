package dam2021.mp08.uf1.practica1;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "patata")
public class Patata {
    @Element(name = "id")
    private String id;
    @Element(name = "nom")
    private String nom;
    @Element(name = "descripcio")
    private String descripcio;
    @Element(name = "sembrar")
    private String sembrar;
    @Element(name = "recollir")
    private String recollir;

    public Patata(String id, String nom, String descripcio, String sembrar, String recollir) {
        this.id = id;
        this.nom = nom;
        this.descripcio = descripcio;
        this.sembrar = sembrar;
        this.recollir = recollir;
    }

    public Patata(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripció(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getSembrar() {
        return sembrar;
    }

    public void setSembrar(String sembrar) {
        this.sembrar = sembrar;
    }

    public String getRecollir() {
        return recollir;
    }

    public void setRecollir(String recollir) {
        this.recollir = recollir;
    }
}
