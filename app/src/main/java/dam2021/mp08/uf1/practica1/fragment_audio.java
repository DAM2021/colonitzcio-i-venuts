package dam2021.mp08.uf1.practica1;

import android.os.Bundle;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.material.snackbar.Snackbar;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link fragment_audio #newInstance} factory method to
 * create an instance of this fragment.
 */
public class fragment_audio extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public fragment_audio() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment fragment_audio.
     */
    // TODO: Rename and change types and number of parameters
    public static fragment_audio newInstance(String param1, String param2) {
        fragment_audio fragment = new fragment_audio();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio, container, false);

        Button buttonPlay = (Button) view.findViewById(R.id.play_button);

        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Play", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
                MediaPlayer so = MediaPlayer.create(getActivity(), R.raw.canco);
                so.start();
            }
        });

        Button buttonStop = (Button) view.findViewById(R.id.stop_button);
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Stop", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
                MediaPlayer so = MediaPlayer.create(getActivity(), R.raw.canco);
                so.stop();
            }
        });
        return view;
    }
    public void onViewCreated( View view, Bundle savedInstanceState) {
        view.findViewById(R.id.llista).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fragment_audio.this)
                        .navigate(R.id.action_fragment_audio_to_LlistaPatatesFragment);
            }
        });
    }
}
