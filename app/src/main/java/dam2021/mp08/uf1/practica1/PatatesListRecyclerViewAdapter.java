package dam2021.mp08.uf1.practica1;

import dam2021.mp08.uf1.practica1.LlistaPatatesFragment.OnListFragmentInteractionListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PatatesListRecyclerViewAdapter extends RecyclerView.Adapter<PatatesListRecyclerViewAdapter.ViewHolder> {

    private final List<Patata> llistaPatates;
    private final OnListFragmentInteractionListener mListener;

    public PatatesListRecyclerViewAdapter(List<Patata> items, OnListFragmentInteractionListener listener) {
        llistaPatates = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_llista_patates, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.patata = llistaPatates.get(position);
        holder.mIdView.setText(holder.patata.getId());
        holder.mNomView.setText(holder.patata.getNom());
        holder.mDescripcioView.setText(holder.patata.getDescripcio());
        holder.mSembrarView.setText(holder.patata.getSembrar());
        holder.mRecollirView.setText(holder.patata.getRecollir());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.patata);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return llistaPatates.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mNomView;
        public final TextView mDescripcioView;
        public final TextView mSembrarView;
        public final TextView mRecollirView;
        public Patata patata;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.patata_id);
            mNomView = (TextView) view.findViewById(R.id.patata_nom);
            mDescripcioView = (TextView) view.findViewById(R.id.patata_descripcio);
            mSembrarView = (TextView) view.findViewById(R.id.patata_sembrar);
            mRecollirView = (TextView) view.findViewById(R.id.patata_recollir);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNomView.getText() + "'";
        }
    }
}
